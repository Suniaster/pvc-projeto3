# Trabalho 3 Visão Computacional

Trabalho 3 da matéria de Principios da visão computacional 2019/1
Feito por Thiago Chaves Monteiro e Larissa Santana de Freitas Andrade
Link do repositório: https://gitlab.com/Suniaster/pvc-projeto3

## Requisitos

Para o uso do progama é necessário que a instalação do software python na versão 3.5.2 tenha sido feito na maquina, assim como da biblioteca opencv, versão 3.3.1, e pyplotib.

## Especificações

O programa é divido em 3 módulos. O primeiro módulo consiste na criação de mapas de profundidade e disparidade da imagem selecionada durante a execução desse módulo. Após fazer os calculos e mostar os resultados, é escrito no terminal como fazer a conversão dos valores mostrados visualmente para medidas em milimetros.

Já o segundo módulo usa como base as imagens do Morpheus dentro da pasta "/data/FurukawaPonce/". Diferentemente do primeiro módulo, as imagens usadas não estão retificadas, então são feitos os calculos necessários e mostrados em novas janelas os resultados para os mapas de disparidade e profunidade.

Por fim, o último módulo, tem um foco mais interativo com o usuário. Após a imagem aparecer na tela, é necessário que sejam selecionados dois pontos para que seja calculado o volume de um paralelepipedo tendo esses pontos como vértices opostos. Esses pontos seriam como os extremos da reta "d" da figura seguinte: 

<img src="./data/paralelepipedo.png"
     style="float: center; margin-right: 10px;" />

Toda vez que um novo ponto é selecionado, as informações sobre eles são escritas no terminal, e quando for possivel calcular o volume do paralelepipedo, também será escrito no terminal as informações dele, com todas as medidas em mm.

## Utilização

Para usar qualquer um dos módulos, basta rodar o arquivo src/pd3.py passando como flags a string --r*n*, substituindo *n* pelo número do módulo desejado.
Exemplo para rodar o módulo 1:

```bash
python src/pd3.py --r1
```

Exemplo para rodar o módulo 3:

```bash
python src/pd3.py --r3
```