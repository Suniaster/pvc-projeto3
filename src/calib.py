import numpy as np
import cv2

def beaultify_mat(str_mtx):
    str_mtx = str_mtx.replace('[','')
    str_mtx = str_mtx.replace(']','')
    
    mt1, mt2, mt3 = str_mtx.split('; ')

    mt1 = np.fromstring(mt1, sep=' ')
    mt2 = np.fromstring(mt2, sep=' ')
    mt3 = np.fromstring(mt3, sep=' ')

    return np.stack((mt1,mt2,mt3))



def read_calib(path_to_folder):
    f = open(path_to_folder+'calib.txt', 'r')
    calib = {}
    for line in f.readlines():
        key, value = line.split('=')
        if key == 'cam1' or key == 'cam0' :
            value = beaultify_mat(value)
        else:
            value = float(value)

        calib[key] = value
    return calib