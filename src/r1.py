import numpy as np
import cv2
from matplotlib import pyplot as plt
from calib import read_calib
import sys

def main_r1():
    try:
        w_size = int(input('Tamanho da window: ')) 
    except:
        print("Entrada invalida")
        sys.exit(1)
    results_path = 'data/results/'
    try: 
        print("(1) Motorcycle\n(2) JadePlant")
        choice = int(input("Escolha a imagem: "))
    except:
        print("Entrada Invalida")
        sys.exit(1)
    if(choice == 1):
        path_to_folder = 'data/Middlebury/Motorcycle-perfect/'
    else:
        path_to_folder = 'data/Middlebury/Jadeplant-perfect/'
    calib = read_calib(path_to_folder)

    ## Lendo imagens
    imgL = cv2.imread(path_to_folder+'im0.png',0)
    imgR = cv2.imread(path_to_folder+'im1.png',0)

    # SGBM Parameters -----------------
    window_size = 3                     # wsize default 3; 5; 7 for SGBM reduced size image; 15 for SGBM full size image (1300px and above); 5 Works nicely
    
    left_matcher = cv2.StereoSGBM_create(
        minDisparity=0,
        numDisparities=int(calib['ndisp']),             # max_disp has to be dividable by 16 f. E. HH 192, 256
        blockSize=w_size,
        P1=8 * 3 * window_size ** 2,    # wsize default 3; 5; 7 for SGBM reduced size image; 15 for SGBM full size image (1300px and above); 5 Works nicely
        P2=32 * 3 * window_size ** 2,
        disp12MaxDiff=1,
        uniquenessRatio=15,
        speckleWindowSize=0,
        speckleRange=2,
        preFilterCap=63,
        mode=cv2.STEREO_SGBM_MODE_SGBM_3WAY
    )
    right_matcher = cv2.ximgproc.createRightMatcher(left_matcher)
    # FILTER Parameters
    lmbda = 80000
    sigma = 1.2
    visual_multiplier = 1.0
    
    wls_filter = cv2.ximgproc.createDisparityWLSFilter(matcher_left=left_matcher)
    wls_filter.setLambda(lmbda)
    wls_filter.setSigmaColor(sigma)

    print('Computando disparidade...')
    displ = left_matcher.compute(imgL, imgR)  # .astype(np.float32)/16
    dispr = right_matcher.compute(imgR, imgL)  # .astype(np.float32)/16q
    displ = np.int16(displ)
    dispr = np.int16(dispr)
    filteredImg = wls_filter.filter(displ, imgL, None, dispr)  # important to put "imgL" here!!!

    # filteredImg = cv2.normalize(src=filteredImg, dst=filteredImg, beta=0, alpha=255, norm_type=cv2.NORM_MINMAX);
    disparity = filteredImg.copy()

    disparity = disparity.astype(float)
    
    # Salvando localização dos pixeis negativos
    NegDisp = np.where((disparity<0))

    f = calib['cam0'][0][0]
    baseline =calib['baseline'] 
    doffs = calib['doffs'] 
    width = calib['width']
    disparity =  disparity/16
    # Caluclando profundidade
    depth = baseline*f/(disparity+doffs)
    # depth = cv2.normalize(src=depth, dst=depth, beta=0, alpha=255, norm_type=cv2.NORM_MINMAX)
    depth[NegDisp] = 0

    ## Mostrando resultados
    plt.imshow(disparity, 'jet')
    plt.title('Mapa de Disparidade')
    plt.colorbar()
    plt.show()
    

    maxValue = np.amax(depth)
    minValue = np.amin(depth)
    print('Para converter os valores de volta para mm, faça:')
    print('Distancia = (ValorDoPix - '+str(minValue)+')/('+str(maxValue)+' - '+str(minValue)+')')
    depth = cv2.normalize(src=depth, dst=depth, beta=0, alpha=254, norm_type=cv2.NORM_MINMAX)
    depth[NegDisp] = 255
    plt.imshow(depth, 'jet')
    plt.title('Mapa de Profundidade')
    plt.colorbar()
    plt.show()

    truth = cv2.imread(path_to_folder+'disp0-n.pgm',0)

    # truth= cv2.normalize(src=truth, dst=truth, beta=0, alpha=255, norm_type=cv2.NORM_MINMAX)
    # disparity = cv2.normalize(src=disparity, dst=disparity, beta=0, alpha=255, norm_type=cv2.NORM_MINMAX)

    # plt.imshow(truth, 'jet')
    # plt.show()
    # dif = np.fabs(truth - disparity)
    # plt.imshow(dif, 'gray')
    # plt.show()


    cv2.namedWindow('profundidade', cv2.WINDOW_NORMAL)
    depth = depth.astype(np.uint8)
    cv2.imshow('profundidade', depth)

    cv2.waitKey(0)

    cv2.imwrite(results_path+'disparidade.pgm', disparity)

