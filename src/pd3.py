import sys
from r1 import main_r1
from r2 import main_r2
from r3 import main_r3
if __name__ == '__main__':
    executado = False
    for arg in sys.argv:
        if(arg == '--r1'):
            print('Executando requisito 1')
            ### Executa a main do requisito 1 ###
            main_r1()
            executado = True
        if(arg == '--r2'):
            print('Executando requisito 2')
            main_r2()
            executado = True
        if(arg == '--r3'):
            print('Executando requisito 3')
            main_r3()
            executado = True

    if not executado :
        print('Erro no argumento, terminando execução...')
        sys.exit(1)