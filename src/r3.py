import numpy as np
import cv2
import math
from matplotlib import pyplot as plt
from calib import read_calib
import sys

cliques = []
TERMINACOR = "\033[0m"
VERMELHO_I = "\033[1;31m"
VERDE_I = "\n\033[1;32m"
def printVerde(str_):
    print(VERDE_I+str_+TERMINACOR)
def printVermelho(str_):
    print(VERMELHO_I+str_+TERMINACOR)

def calculateWorldCord(pointVector, Plinv):
        worldCord = np.matmul(Plinv,pointVector)
        for i in range(3):
            worldCord[i] /= worldCord[2]
        return worldCord

def Caixa(event,x,y,flags,param):
    if event == cv2.EVENT_LBUTTONDOWN:
        print(x,y)
        global cliques
        if len(cliques) <= 2:
            cliques.append((x,y))
        if len(cliques) == 2:
            # Pegando parametros
            img = param[0]
            depth = param[1]
            Pdlinv = param[2]

            # Calculando coordenadas do mundo
            coord1 = [cliques[0][0], cliques[0][1], 1]
            coord2 = [cliques[1][0], cliques[1][1], 1]
            # Valores das coordendadas no mundo
            w_coord1 = calculateWorldCord(coord1, Pdlinv)
            w_coord2 = calculateWorldCord(coord2, Pdlinv)

            depth = depth.astype(int)
            # Fazendo os calculos
            comprimento = math.fabs(w_coord1[0] - w_coord2[0])
            altura = math.fabs(w_coord1[1] - w_coord2[1])
            print(depth[cliques[0]])
            print(depth[cliques[1]])
            profundidade = math.fabs(depth[cliques[0]] - depth[cliques[1]])

            volume = comprimento*altura*profundidade

            # Mostrando resultado
            printVermelho("O comprimento é de :" + str(comprimento))
            printVermelho("A altura é de : "+ str(altura))
            printVermelho("A profundidade é de : " + str(profundidade))
            printVerde('--> O volume é: ' + str(volume))
            cliques = []


def main_r3():

    results_path = 'data/results/'

    depth = CalculaDepth()
    img = cv2.imread(results_path+'MorpheusLRectfied.jpg',-1)

    mtx = np.array([[6704.926882,0.000103 , 738.251932], [0, 6705.241311, 457.560286 ], [0,0,1]]) 

    rmtx = np.array([ [0.70717199,  0.70613396, -0.03581348] ,
                [0.28815232, -0.33409066, -0.89741388 ],
                [-0.64565936,  0.62430623, -0.43973369] ])
    tvecs = np.array([[ -532.285900 ], [207.183600] , [ 2977.408000 ]])

    K = np.concatenate((rmtx,tvecs), axis=1)

    # Caluclos do P para matriz distorcida
    Pd = np.matmul(mtx, K)
    Pdl = np.delete(Pd,2,axis=1)
    Pdlinv=np.linalg.inv(Pdl)



    cv2.namedWindow('Morpheus', cv2.WINDOW_NORMAL)
    cv2.imshow('Morpheus', img)
    cv2.setMouseCallback('Morpheus', Caixa, [img, depth, Pdlinv])
    cv2.waitKey(0)


def CalculaDepth():
    results_path = 'data/results/'
    path_to_folder = 'data/FurukawaPonce/'

    ## Lendo imagens 
    img1 = cv2.imread(path_to_folder+'MorpheusL.jpg',0)
    img2 = cv2.imread(path_to_folder+'MorpheusR.jpg',0)

    #Cortando imagem
    img1 = cv2.resize(img1,img2.shape)

    sift = cv2.xfeatures2d.SIFT_create()
    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(img1,None)
    kp2, des2 = sift.detectAndCompute(img2,None)

    # FLANN parameters
    FLANN_INDEX_KDTREE = 1
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks=50)
    flann = cv2.FlannBasedMatcher(index_params,search_params)
    matches = flann.knnMatch(des1,des2,k=2)
    good = []
    pts1 = []
    pts2 = []
    # ratio test as per Lowe's paper
    for i,(m,n) in enumerate(matches):
        if m.distance < 0.8*n.distance:
            good.append(m)
            pts2.append(kp2[m.trainIdx].pt)
            pts1.append(kp1[m.queryIdx].pt)

    pts1 = np.int32(pts1)
    pts2 = np.int32(pts2)

    #Getting fundamental matrix
    F, mask = cv2.findFundamentalMat(pts1,pts2,cv2.FM_RANSAC)
    # We select only inlier points
    pts1 = pts1[mask.ravel()==1]
    pts2 = pts2[mask.ravel()==1]

    # Achando matrizes de homografia
    _ , h1, h2 = cv2.stereoRectifyUncalibrated(pts1,pts2,F,img1.shape,threshold=3 )

    # Calculando matriz de rotação para retificar
    mtx1 = np.array([[6704.926882,0.000103 , 738.251932], [0, 6705.241311, 457.560286 ], [0,0,1]]) 
    mtx1_1 = np.linalg.inv(mtx1)
    R1 = np.matmul(h1, mtx1)
    R1 = np.matmul(mtx1_1,R1)

    # Calculando matriz de rotação para retificar
    mtx2 = np.array([[6682.125964,0.000101 , 875.207200], [0, 6681.475962, 357.700292 ], [0,0,1]])
    mtx2_1 = np.linalg.inv(mtx2)
    R2 = np.matmul(h2, mtx2)
    R2 = np.matmul(mtx2_1,R2)

    distCoefs = np.zeros((5,1))

    # Pegando mapeamentos
    map11, map21 = cv2.initUndistortRectifyMap(mtx1,distCoefs, R1,mtx1,img1.shape, cv2.CV_32F)
    map12, map22 = cv2.initUndistortRectifyMap(mtx2,distCoefs, R2,mtx2,img2.shape, cv2.CV_32F)

    # Remapeando imagens
    im1_remaped = cv2.remap(img1,map11,map21,1)
    im2_remaped = cv2.remap(img2, map12, map22,1)

    window_size = 5
    #Cortando imagem
    left_matcher = cv2.StereoSGBM_create(
        minDisparity=0,
        numDisparities=160,             # max_disp has to be dividable by 16 f. E. HH 192, 256
        blockSize=5,
        P1=8 * 3 * window_size ** 2,    # wsize default 3; 5; 7 for SGBM reduced size image; 15 for SGBM full size image (1300px and above); 5 Works nicely
        P2=32 * 3 * window_size ** 2,
        disp12MaxDiff=1,
        uniquenessRatio=1,
        speckleWindowSize=0,
        speckleRange=2,
        preFilterCap=63,
        mode=cv2.STEREO_SGBM_MODE_SGBM_3WAY
    )
    right_matcher = cv2.ximgproc.createRightMatcher(left_matcher)
    # FILTER Parameters
    lmbda = 80000
    sigma = 1.2
    visual_multiplier = 1.0

    wls_filter = cv2.ximgproc.createDisparityWLSFilter(matcher_left=left_matcher)
    wls_filter.setLambda(lmbda)
    wls_filter.setSigmaColor(sigma)

    displ = left_matcher.compute(im1_remaped, im2_remaped)  # .astype(np.float32)/16
    dispr = right_matcher.compute(im2_remaped, im1_remaped)  # .astype(np.float32)/16
    displ = np.int16(displ)
    dispr = np.int16(dispr)
    filteredImg = wls_filter.filter(displ, im1_remaped, None, dispr)  # important to put "imgL" here!!!

    disparity = filteredImg
    disparity = disparity.astype(float)
    disparity = disparity/16

    R_2 = np.array([ [0.48946344,  0.87099159, -0.04241701] ,
            [0.33782142, -0.23423702, -0.91159734] ,
            [-0.80392924,  0.43186419, -0.40889007] ])

    R_2 = cv2.Rodrigues(R_2)[0]

    T_2 = np.array([ -614.549000 , 193.240700 , 3242.754000 ])

    R_1 = np.array([ [0.70717199,  0.70613396, -0.03581348] ,
                [0.28815232, -0.33409066, -0.89741388 ],
                [-0.64565936,  0.62430623, -0.43973369] ])

    R_1 = cv2.Rodrigues(R_1)[0]

    T_1 = np.array([ -532.285900 , 207.183600 , 2977.408000 ])

    T_3 = cv2.composeRT(R_1,T_1,R_2,T_2)[1]

    # Mudei o calculo da base line pq T_3 ta dando um valor 
    # absurdo
    baseline = np.linalg.norm(T_1-T_2)
    f = 6704.926882
    c1 = np.array([ 738.251932, 457.560286 ])
    c2 = np.array([ 875.207200, 357.700292 ])
    doffs = np.linalg.norm(c1-c2)

    depth =  baseline*f / (disparity+doffs)

    return depth